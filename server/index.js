import express from "express";
import dotenv from "dotenv";

const app = express();
dotenv.config();

const PORT = process.env.PORT;

app.get("/", (req, res) => {
    return res.send("Hello world from Nhàbáo");
});

app.listen(PORT, () => {
    console.log("Nhàbáo is running")
})