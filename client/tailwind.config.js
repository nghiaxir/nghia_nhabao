/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{js,ts,jsx,tsx}"
    ],
    theme: {
        extend: {
            spacing: {
                'xl': '80rem',
            }
        },
    },
    plugins: [
        require('@tailwindcss/line-clamp'),
    ],
}