import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import className from "./className";


function SearchBar() {

    const navigate = useNavigate();

    const [searchValue, setSearchValue] = useState("");

    return (
        <div className={className.wrapper}>
            <div className={className.border}>
                <input
                    className={className.inputSearch}
                    value={searchValue}
                    onChange={e => setSearchValue(e.target.value)}
                    placeholder={"Nhập lội dung tìm kiếm"}
                    onKeyDown={e => {
                        if (e.key == "Enter") {
                            navigate(`/search?q=${searchValue}`)
                        }
                    }} />
                <div
                    className={className.buttonSearch}
                    onClick={() => navigate(`/search?q=${searchValue}`)}>
                    <FontAwesomeIcon
                        icon={faMagnifyingGlass}
                        className={className.iconSearch} />
                </div>

            </div>
        </div>
    );
}

export default SearchBar