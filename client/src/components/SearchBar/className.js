import Utils from "../../utils/Utils"

const className = {
    wrapper: "flex-1 flex flex-row mx-6",
    border: " flex-1 flex flex-row items-center h-8 border-2",
    inputSearch: "flex-1 block ml-2 text-sm",
    buttonSearch: [
        "flex items-center cursor-pointer justify-center",
        "hover:bg-slate-300 h-7 w-8"
    ],
    iconSearch: [
        "text-black mr-2 ",
        "ml-2"
    ]
}

Utils.bind(className)

export default className