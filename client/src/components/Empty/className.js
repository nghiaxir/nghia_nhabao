import Utils from "../../utils/Utils";

const className = {
    container: [
        "flex flex-col items-center",
        "fixed top-0 right-0 left-0 bottom-0",
    ],
    content: [
        "w-full h-full flex-col md:w-[600px]",
        "flex items-center justify-center"
    ],
    image: [
        "w-full"
    ],
    title: [
        "text-lg"
    ],
    button: [
        "bg-red-500 mt-4 cursor-pointer",
        "p-3 rounded-md text-white text-base",
        "hover:bg-red-400"
    ]
}

Utils.bind(className);

export default className