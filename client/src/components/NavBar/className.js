import Utils from "../../utils/Utils"

const className = {
    wrapper: "w-screen bg-blue-400 flex flex-col items-center",
    content: "mx-auto w-screen flex flex-row h-9 xl:w-xl",
    navItem: [
        "flex items-center justify-center",
        "cursor-pointer h-9 p-4 min-w-[6rem]",
        "text-white font-medium hover:bg-blue-500"
    ],
    active: [
        " bg-blue-500"
    ]
}

Utils.bind(className)

export default className