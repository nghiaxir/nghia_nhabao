import { useNavigate } from "react-router-dom";

import config from "../../utils/Config";
import className from "./className";

const navBars = [
    {
        id: 0,
        display: "TRANG CHỦ",
        path: "/"
    },
    {
        id: 1,
        display: "MỚI",
        path: "/newest"
    },
    {
        id: 2,
        display: "VIDEO",
        path: "/video"
    },
    {
        id: 3,
        display: "CHỦ ĐỀ",
        path: "/category"
    },
]

function NavBar() {

    const navigate = useNavigate();

    const getCurrentIDPage = () => {
        const url = window.location.href;
        if (url == config.baseURL) {
            return 0;
        }
        for (const item of navBars) {
            const index = url.indexOf(item.path.slice(1, item.path.length));
            if (index > 0) {
                return item.id;
            }
        }
        return -1;
    }

    const idCurrentPage = getCurrentIDPage();

    const navs = navBars.map(item => ({
        ...item,
        active: item.id == idCurrentPage
    }));

    return (
        <div className={className.wrapper}>
            <div className={className.content}>
                {navs.map(item => (
                    <div
                        key={item.id}
                        className={className.navItem + (item.active ? className.active : "")}
                        onClick={() => navigate(item.path)}>
                        {item.display}
                    </div>
                ))}
            </div>
        </div>
    );
}

export default NavBar