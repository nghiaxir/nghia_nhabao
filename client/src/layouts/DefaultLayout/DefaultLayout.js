import Header from "../../components/Header"
import NavBar from "../../components/NavBar"

import styles from "./styles.scss"

function DefaultLayout({ children }) {

    return (
        <div>
            <Header />
            <NavBar />
            <div className="w-screen flex flex-col items-center">
                {children}
            </div>
        </div>
    );
}

export default DefaultLayout
export { styles }