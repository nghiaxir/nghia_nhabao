import { BrowserRouter as Router, Route, Routes } from "react-router-dom"
import DefaultLayout from './layouts/DefaultLayout';
import Home from './pages/Home';
import Video from './pages/Video';
import Search from './pages/Search';
import Newest from "./pages/Newest";
import Paper from "./pages/Paper";
import Category from "./pages/Category";
import SubCategory from "./pages/SubCategory";
import Empty from "./components/Empty";
import ScrollToTop from "./components/ScrollToTop";

function App() {
    return (
        <Router>
            <div className="lex-col items-center overflow-x-hidden">
                <Routes>
                    <Route
                        path='/'
                        element={
                            <DefaultLayout>
                                {<Home />}
                            </DefaultLayout>
                        } />
                    <Route
                        path='/video'
                        element={
                            <DefaultLayout>
                                {<Video />}
                            </DefaultLayout>
                        } />
                    <Route
                        path='/search'
                        element={
                            <DefaultLayout>
                                {<Search />}
                            </DefaultLayout>
                        } />
                    <Route
                        path='/newest'
                        element={
                            <DefaultLayout>
                                {<Newest />}
                            </DefaultLayout>
                        } />
                    <Route
                        path='/paper'
                        element={
                            <DefaultLayout>
                                {<Paper />}
                            </DefaultLayout>
                        } />
                    <Route
                        path='/category'
                        element={
                            <DefaultLayout>
                                {<Category />}
                            </DefaultLayout>
                        } />
                    <Route
                        path='/sub-category'
                        element={
                            <DefaultLayout>
                                {<SubCategory />}
                            </DefaultLayout>
                        } />
                    <Route
                        path="*"
                        element={<Empty />} />
                </Routes>
                <ScrollToTop />
            </div>
        </Router>
    );
}

export default App;
