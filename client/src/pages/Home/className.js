import Utils from "../../utils/Utils";

const className = {
    container: [
        "w-screen xl:w-xl flex flex-row pt-4"
    ],
    content: [
        "flex flex-col w-full px-4",
        "xl:w-8/12 xl:px-0"
    ]
}

Utils.bind(className);

export default className