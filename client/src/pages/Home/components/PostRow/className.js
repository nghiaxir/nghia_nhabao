import Utils from "../../../../utils/Utils";

const className = {
    wrapper: [
        "flex flex-col w-full",
        "md:flex-row md:-mx-1"
    ],
}

Utils.bind(className);

export default className