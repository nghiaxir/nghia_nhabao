import className from "./className"

function Post({ id, title, image }) {

    return (
        <div className={className.wrapper}>
            <img
                className={className.image}
                src={image} />
            <div className={className.content}>
                <div className={className.title}>
                    {title}
                </div>
                <div className={className.infor}>
                    <img
                        className={className.logoPaper}
                        src={"https://photo-baomoi.bmcdn.me/bb071048550abc54e51b.png"} />
                    <div className={className.time}>{"2 giờ trước"}</div>
                </div>
            </div>
        </div>
    )
}

export default Post