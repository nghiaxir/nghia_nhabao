import SideBar from "../../components/SideBar";
import className from "./className";
import PostMain from "./components/PostMain";


const posts = [
    {
        id: 0,
        title: " Ông Biden lên tiếng sau thắng lợi của đảng Dân chủ ở Thượng viện",
        image: "https://photo-baomoi.bmcdn.me/w300_r3x2/2022_11_13_119_44259652/7784887dbb30526e0b21.jpg",
        link: ""
    },
    {
        id: 1,
        title: "Thủ tướng Chính phủ Phạm Minh Chính gặp Thủ tướng Nhật Bản Kishida Fumio",
        image: "https://photo-baomoi.bmcdn.me/w300_r3x2/2022_11_13_294_44259222/abafa85f9b12724c2b03.jpg",
        link: ""
    },
    {
        id: 2,
        title: "Chelsea thua liểng xiểng, fan tức giận đòi sa thải Graham Potter",
        image: "https://photo-baomoi.bmcdn.me/w300_r3x2_sm/2022_11_13_23_44258856/7a6ecd82fecf17914ede.jpg",
        link: ""
    },
    {
        id: 3,
        title: "Bộ trưởng Bùi Thanh Sơn gặp Bộ trưởng Ngoại giao Nga và Ukraine",
        image: "https://photo-baomoi.bmcdn.me/w300_r3x2/2022_11_13_65_44258844/4389de65ed2804765d39.jpg",
        link: ""
    },
    {
        id: 4,
        title: "Thủ tướng Phạm Minh Chính dự Hội nghị cấp cao Đông Á lần thứ 17",
        image: "https://photo-baomoi.bmcdn.me/w300_r3x2/2022_11_13_294_44259882/884f59b76afa83a4daeb.jpg",
        link: ""
    },
    {
        id: 5,
        title: "Thủ tướng Chính phủ Phạm Minh Chính gặp Thủ tướng Nhật Bản Kishida Fumio",
        image: "https://photo-baomoi.bmcdn.me/w300_r3x2/2022_11_13_294_44259222/abafa85f9b12724c2b03.jpg",
        link: ""
    },
    {
        id: 6,
        title: "Chelsea thua liểng xiểng, fan tức giận đòi sa thải Graham Potter",
        image: "https://photo-baomoi.bmcdn.me/w300_r3x2_sm/2022_11_13_23_44258856/7a6ecd82fecf17914ede.jpg",
        link: ""
    },
    {
        id: 7,
        title: "Bộ trưởng Bùi Thanh Sơn gặp Bộ trưởng Ngoại giao Nga và Ukraine",
        image: "https://photo-baomoi.bmcdn.me/w300_r3x2/2022_11_13_65_44258844/4389de65ed2804765d39.jpg",
        link: ""
    },
    {
        id: 8,
        title: "Thủ tướng Phạm Minh Chính dự Hội nghị cấp cao Đông Á lần thứ 17",
        image: "https://photo-baomoi.bmcdn.me/w300_r3x2/2022_11_13_294_44259882/884f59b76afa83a4daeb.jpg",
        link: ""
    },
]

function Home() {

    return (
        <div className={className.container}>
            <div className={className.content}>
                <PostMain
                    posts={posts} />
            </div>
            <SideBar />
        </div>
    )
}

export default Home