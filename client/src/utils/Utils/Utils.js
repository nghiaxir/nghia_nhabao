import _ from "lodash";

const bind = (obj = {}) => {
    _.entries(obj).forEach(([key, value]) => {
        if (_.isArray(value)) {
            obj[key] = value.join(" ");
        }
    });
}

const Utils = {
    bind
}

export default Utils